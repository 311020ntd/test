var express = require('express');
var router = express.Router();

const mongoose = require("mongoose");

mongoose.set('strictQuery', false);
mongoose.connect("mongodb://localhost:27017/project", { useNewUrlParser: true });

const hoatdongSchema = mongoose.Schema({
  MaHD:{
    type: String,
  },
  TenHD:{
    type: String,
  },
  MoTaHD:{
    type: String,
  },
  NgayGioHD:{
    type: String,
  },
  NgayGioKT:{
    type: String,
  },
  SLToiThieuYC:{
    type: String,
  },
  SLToiDaYC:{
    type: String,
  },
  ThoiHanDK:{
    type: String,
  },
  TrangThai:{
    type: String,
  },
  MaTV:{
    type: String,
  },
  LyDoHuyHD:{
    type: String,
  },
});

const hoatdong = mongoose.model("hoatdongs", hoatdongSchema);

router.get('/', function(req, res) {
  hoatdong.find({},(error, data)=>{
    // console.log('danh sach hoat dong', data)
    res.render('index',{hoatdong: data})
  })
})

router.get('/form-add',function(req,res){
  res.render('form-add',{})
})

router.post("/add",function(req,res){
  hoatdong.create(req.body)
  res.redirect('/')
})

router.get('/form-edit/:id',function(req,res){
  hoatdong.findById({_id:req.params.id},(error,data)=>{
    res.render('form-edit',{hoatdong: data});
  })
})

router.post('/edit/:id',function(req,res){
  console.log({_id: req.body.id})
  console.log(req.body)
  hoatdong.findByIdAndUpdate({_id: req.body.id}, req.body, {new: true}, (error,data)=>{
    res.redirect('/')
  })
})

router.get('/form-delete/:id',function(req,res){
  hoatdong.findByIdAndDelete(req.params.id,(error,data)=>{
    res.redirect('/')
  })
})


const thamgiaSchema = mongoose.Schema({
  MaTV:{
    type: String,
  },
  MaHD:{
    type: String,
  },
  NgayGioDangKy:{
    type: String,
  },
  DiemTruongDoan:{
    type: String,
  },
  DiemTieuChi1:{
    type: String,
  },
  DiemTieuChi2:{
    type: String,
  },
  DiemTieuChi3:{
    type: String,
  },
  DiemTieuChiTrungBinh:{
    type: String,
  },
  NhanXetKhac:{
    type: String,
  },
});

const thamgia = mongoose.model("thamgias", thamgiaSchema);

router.get('/thamgia', function(req, res, next) {
  thamgia.find({},(error, data)=>{
    console.log({DiemTieuChiTrungBinh: data.DiemTieuChi1})
    res.render('thamgia',{thamgia: data})
  })
  thamgia.findById({DiemTieuChiTrungBinh: req.body.DiemTieuChi1+req.body.DiemTieuChi2+req.body.DiemTieuChi3})
})





const thanhvienSchema = mongoose.Schema({
  MaTV:{
    type: String,
  },
  HoTen:{
    type: String,
  },
  GioiTinh:{
    type: String,
  },
  NgaySinh:{
    type: String,
  },
  DiaChiEmail:{
    type: String,
  },
  SoDienThoai:{
    type: String,
  },
});

const thanhvien = mongoose.model("thanhviens", thanhvienSchema);

module.exports = router;
